#include "lsystem.hpp"

#include <cstdio>
#include <stack>
#include <string>
#include <random>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/norm.hpp>


namespace {
    int uniform_rand(int a, int b) {
        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(a, b);
        return distribution(generator);
    }
}

LSystem::LSystem() {
    angle         = 30.0f * glm::pi<float>() / 180.0f;
    distScale     = 0.9;
    angleScale    = 1.1;
}

void LSystem::interpretString(std::string str) {
    State                   state;
    std::stack<State>       st;
    glm::vec3               d;

    state.pos    = glm::vec3(0, 0, 0);
    state.dir    = glm::vec3(0, 1, 0);
    state.angles = glm::vec3(0, 0, 0);
    state.angle  = angle;
    state.invert = 1;
    
    for (auto c : str) {
        switch (c) {
        case '[':       // push state
            st.push(state);
            break;

        case ']':       // pop state
            state = st.top();
            st.pop();
            break;

        case 'F':
            d = step(state);
            lines.push_back({static_cast<int>(st.size()), state.pos, state.pos + d});
            updateState(state, d, st.size());
            break;

        case 'f':
            updateState(state, step(state), st.size());
            break;

        case '!':                       // inverse + and - meaing (just as for & and ^ and < and >)
            state.invert *= -1;
            break;
            
        case '+':
            state.angles.z += state.invert * state.angle;
            break;

        case '-':
            state.angles.z -= state.invert * state.angle;
            break;
            
        case '&':
            state.angles.x += state.invert * state.angle;
            break;

        case '^':
            state.angles.x -= state.invert * state.angle;
            break;
            
        case '<':
            state.angles.y += state.invert * state.angle;
            break;

        case '>':
            state.angles.y -= state.invert * state.angle;
            break;
        }
    }
}

void LSystem::buildSystem(int numIterations) {
    currentString = initialString;
    
    for (int i = 0; i < numIterations; ++i) {
        currentString = oneStep(currentString);
    }
}

std::string LSystem::oneStep(std::string in) const {
    std::string out;

    for (auto c : in) {
        auto it = rules.find(c);
        if (it != rules.end()) {
            const auto& crules = it->second;
            int chosen_rule = uniform_rand(0, static_cast<int>(crules.size()) - 1);
            out += crules[chosen_rule];
        } else {
            out += c;
        }
    }

    return out;
}

void LSystem::updateState(State& state, const glm::vec3& dir, int level) const {
    state.pos   += dir;
    state.dir   *= distScale;
    state.angle *= angleScale;
}

glm::vec3 LSystem::step(const State& state) const {
    auto ret = glm::rotateX(state.dir, state.angles.x);
    ret = glm::rotateY(ret, state.angles.y);
    ret = glm::rotateZ(ret, state.angles.z);
    return ret;
}
