#pragma once

#include <vector>

#include "common/Mesh.hpp"
#include "lsystem.hpp"

std::pair<MeshPtr, MeshPtr> makeTreeAndLeaves(const std::vector<LSystemLine>& lines);

