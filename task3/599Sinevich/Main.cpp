#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <cassert>

#include <iostream>
#include <vector>
#include <random>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/LightInfo.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"

#include "lsystem.hpp"
#include "tree.hpp"

namespace {

float frand(float a, float b) {

    static std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution(a, b);

    return distribution(generator);
}

}


class SampleApplication : public Application
{
public:
    const int NUM_TREES = 50;

    std::pair<MeshPtr, MeshPtr> _tree_and_leaves;
    MeshPtr _sphere;
    MeshPtr _ground;

    ShaderProgramPtr _shader;

    // Directional light angle
    float _phi = glm::pi<float>() * 0.0f;
    float _theta = glm::pi<float>() * 0.25f;

    // Directional light parameters
    LightInfo _light;

    TexturePtr _barkTexture;
    TexturePtr _grassTexture;
    TexturePtr _leafTexture;

    GLuint _sampler;
    GLuint _grassSampler;
    GLuint _leafSampler;

    std::vector<glm::vec3> _positionsVec3;

    void makeScene() override {
        Application::makeScene();

        LSystem lsystem;
        lsystem.setInitialString ("F");
        lsystem.addRule('F', "F[-F]&[F]^^[F]&[+F][F]");
        /*
        lsystem.addRule('F', "[--F]&&[F]^^[F][+F]");
        lsystem.setInitialString ("A");
        lsystem.addRule('A', "[F[+FCA][-FCA]]");
        lsystem.addRule('B', "[F[>FCB][<FCB]]");
        */
        lsystem.buildSystem(3);
        _tree_and_leaves = makeTreeAndLeaves(lsystem.draw());
        _tree_and_leaves.first->setModelMatrix(glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec3(0.0f, -2.0f, 0.0f)));
        _tree_and_leaves.second->setModelMatrix(glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.5f)), glm::vec3(0.0f, -2.0f, 0.0f)));

        //=========================================================

        const float size = 20.0f;
        for (unsigned int i = 0; i < NUM_TREES; i++)
        {
            _positionsVec3.push_back(glm::vec3(frand(-size, size), 0, frand(-size, size)));
            std::cerr << _positionsVec3.back().x << " " << _positionsVec3.back().z << std::endl;
        }

        _ground = makeGroundPlane(30.0f, 5.0f);
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
        
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("../task3/599Sinevich/599SinevichData/shader.vert", "../task3/599Sinevich/599SinevichData/shader.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        // Direction pointing outwards, toward the light source
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_theta), glm::sin(_phi) * glm::cos(_theta));
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(0.3, 0.3, 0.3);

        //=========================================================
        //Загрузка и создание текстур
        _barkTexture = loadTexture("../task3/599Sinevich/599SinevichData/bark2.jpg");
        _grassTexture = loadTexture("../task3/599Sinevich/599SinevichData/grass.jpg");
        _leafTexture = loadTexture("../task3/599Sinevich/599SinevichData/leaves3.png");


        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        // Tree bark and branches texture
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // Grass texture
        glGenSamplers(1, &_grassSampler);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_grassSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // Leaves texture
        glGenSamplers(1, &_leafSampler);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_leafSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер		
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));
        // std::cout << lightDirCamSpace.x << " " << lightDirCamSpace.y << " " << lightDirCamSpace.z << std::endl;

        _shader->setVec3Uniform("light.dir", lightDirCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        {
            _shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0        
            glBindSampler(0, _grassSampler);
            _grassTexture->bind();

            _ground->draw();
        }

        {
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _sampler);
            _barkTexture->bind();
            _shader->setIntUniform("diffuseTex", 0);
            _shader->setVec3UniformArray("positions", _positionsVec3);

            //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
            _shader->setMat4Uniform("modelMatrix", _tree_and_leaves.first->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree_and_leaves.first->modelMatrix()))));
            _tree_and_leaves.first->drawInstanced(_positionsVec3.size());
        }

        {
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _leafSampler);
            _leafTexture->bind();
            _shader->setIntUniform("diffuseTex", 0);
            _shader->setVec3UniformArray("positions", _positionsVec3);

            //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
            _shader->setMat4Uniform("modelMatrix", _tree_and_leaves.second->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree_and_leaves.second->modelMatrix()))));
            _tree_and_leaves.second->drawInstanced(_positionsVec3.size());
        }


        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
