#include "tree.hpp"

#include <iostream>

#include "common/Mesh.hpp"


namespace {

void makeCone(const glm::vec3& a,
        const glm::vec3& b,
        float r1,
        float r2,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords) {

    assert(r1 > 0.0 && r2 > 0.0);
    assert(N >= 3);

    auto ab = b - a;
    // Perpendiculars to ab
    auto p1 = normalize(glm::vec3(-ab.y, ab.x, 0));
    auto p2 = normalize(cross(ab, p1));
    auto get_p = [=](float phi, float r) {
        return p1 * glm::cos(phi) * r + p2 * glm::sin(phi) * r;
    };
    for (unsigned int i = 0; i < N; i++) {
        float phi1 = 2.0f * glm::pi<float>() * i / N;
        float phi2 = 2.0f * glm::pi<float>() * (i + 1) / N;

        // Bottom face triangles
        vertices.emplace_back(a);
        vertices.emplace_back(a + get_p(phi1, r1));
        vertices.emplace_back(a + get_p(phi2, r1));

        normals.emplace_back(normalize(a - b));
        normals.emplace_back(normalize(a - b));
        normals.emplace_back(normalize(a - b));

        texcoords.emplace_back(0, 0);
        texcoords.emplace_back(0, 0);
        texcoords.emplace_back(0, 0);

        // Top face triangles
        vertices.emplace_back(b);
        vertices.emplace_back(b + get_p(phi1, r2));
        vertices.emplace_back(b + get_p(phi2, r2));

        normals.emplace_back(normalize(b - a));
        normals.emplace_back(normalize(b - a));
        normals.emplace_back(normalize(b - a));

        texcoords.emplace_back(0, 0);
        texcoords.emplace_back(0, 0);
        texcoords.emplace_back(0, 0);

        // Sides
        auto normal = normalize((get_p(phi1, r1) + get_p(phi1, r1)) / 2.0f);
        for (unsigned int j = 0; j < N; j++) {
            auto c1 = a  + (b - a) * static_cast<float>(j) / static_cast<float>(N);
            auto c2 = a  + (b - a) * static_cast<float>(j + 1) / static_cast<float>(N);
            float cr = r1 + (r2 - r1) * j / N;
            float nr = r1 + (r2 - r1) * (j + 1) / N;

            // First side triangle
            vertices.emplace_back(c1 + get_p(phi1, cr));
            vertices.emplace_back(c1 + get_p(phi2, cr));
            vertices.emplace_back(c2 + get_p(phi2, nr));

            // normals.emplace_back(normal);
            // normals.emplace_back(normal);
            // normals.emplace_back(normal);

            normals.emplace_back(get_p(phi1, 1));
            normals.emplace_back(get_p(phi2, 1));
            normals.emplace_back(get_p(phi2, 1));

            texcoords.emplace_back(static_cast<float>(j) / N, static_cast<float>(i) / N);
            texcoords.emplace_back(static_cast<float>(j) / N, static_cast<float>(i + 1) / N);
            texcoords.emplace_back(static_cast<float>(j + 1) / N, static_cast<float>(i + 1) / N);

            // Second side triangle
            vertices.emplace_back(c2 + get_p(phi1, nr));
            vertices.emplace_back(c2 + get_p(phi2, nr));
            vertices.emplace_back(c1 + get_p(phi1, cr));

            // normals.emplace_back(normal);
            // normals.emplace_back(normal);
            // normals.emplace_back(normal);

            normals.emplace_back(get_p(phi1, 1));
            normals.emplace_back(get_p(phi2, 1));
            normals.emplace_back(get_p(phi1, 1));

            texcoords.emplace_back(static_cast<float>(j + 1) / N, static_cast<float>(i) / N);
            texcoords.emplace_back(static_cast<float>(j + 1) / N, static_cast<float>(i + 1) / N);
            texcoords.emplace_back(static_cast<float>(j) / N, static_cast<float>(i) / N);
        }
    }
}


void makeLeaves(const glm::vec3& a,
        const glm::vec3& b,
        float size,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords) {
    auto ab = b - a;
    auto nab = normalize(ab);
    // Perpendiculars to ab
    auto p1 = normalize(glm::vec3(-ab.y, ab.x, 0));
    auto p2 = normalize(cross(ab, p1));
    auto p3 = normalize(cross(p1, p2));
    p1 *= size;
    p2 *= size;
    p3 *= size;
    for (int i = 1; i <= N; i++) {
        auto c = a + (b - a) * static_cast<float>(i) / static_cast<float>(N);
        vertices.emplace_back(c - p1 - p2);
        vertices.emplace_back(c - p1 + p2);
        vertices.emplace_back(c + p1 + p2);

        normals.emplace_back(nab);
        normals.emplace_back(nab);
        normals.emplace_back(nab);

        texcoords.emplace_back(0.0, 0.0);
        texcoords.emplace_back(0.0, 1.0);
        texcoords.emplace_back(1.0, 1.0);


        vertices.emplace_back(c + p1 - p2);
        vertices.emplace_back(c - p1 - p2);
        vertices.emplace_back(c + p1 + p2);

        normals.emplace_back(nab);
        normals.emplace_back(nab);
        normals.emplace_back(nab);

        texcoords.emplace_back(1.0, 0.0);
        texcoords.emplace_back(0.0, 0.0);
        texcoords.emplace_back(1.0, 1.0);


        vertices.emplace_back(c - p1 - p3);
        vertices.emplace_back(c - p1 + p3);
        vertices.emplace_back(c + p1 + p3);

        normals.emplace_back(nab);
        normals.emplace_back(nab);
        normals.emplace_back(nab);

        texcoords.emplace_back(0.0, 0.0);
        texcoords.emplace_back(0.0, 1.0);
        texcoords.emplace_back(1.0, 1.0);


        vertices.emplace_back(c + p1 - p3);
        vertices.emplace_back(c - p1 - p3);
        vertices.emplace_back(c + p1 + p3);

        normals.emplace_back(nab);
        normals.emplace_back(nab);
        normals.emplace_back(nab);

        texcoords.emplace_back(1.0, 0.0);
        texcoords.emplace_back(0.0, 0.0);
        texcoords.emplace_back(1.0, 1.0);
    }
}

MeshPtr makeMesh(
        const std::vector<glm::vec3>& vertices,
        const std::vector<glm::vec3>& normals,
        const std::vector<glm::vec2>& texcoords
    ) {

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

}


std::pair<MeshPtr, MeshPtr> makeTreeAndLeaves(const std::vector<LSystemLine>& lines) {
    // const float BARK_RADIUS = 0.1f;
    // const float INITIAL_RADIUS = 0.05f;
    // const float RADIUS_SCALE = 0.90f;
    const float BARK_RADIUS = 0.18f;
    const float INITIAL_RADIUS = 0.13f;
    const float RADIUS_SCALE = 0.3f;
    const int DETAIL = 15;
    const int MIN_LEAF_LEVEL = 3;
    const float LEAF_RECT_SIZE = 0.075;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    std::vector<glm::vec3> lvertices;
    std::vector<glm::vec3> lnormals;
    std::vector<glm::vec2> ltexcoords;

    auto get_radius = [=](int level) {
        float radius = level == 0 ? BARK_RADIUS : INITIAL_RADIUS;
        for (int i = 0; i < level - 1; i++) {
            radius *= RADIUS_SCALE;
        }
        return radius;
    };

    for (auto& line : lines) {
        makeCone(line.a, line.b, get_radius(line.level), get_radius(line.level + 1), DETAIL,
                vertices, normals, texcoords);
        if (line.level >= MIN_LEAF_LEVEL) {
            makeLeaves(line.a, line.b, LEAF_RECT_SIZE, 3,
                    lvertices, lnormals, ltexcoords);
        }
    }

    return std::make_pair(
        makeMesh(vertices, normals, texcoords),
        makeMesh(lvertices, lnormals, ltexcoords)
    );
}

